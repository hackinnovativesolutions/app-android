package br.com.rioquenteresorts.rioquentemoments.presentation.evaluation;

/**
 * @author Filipe Bezerra
 */
public class ShowMyInfoEvent {

    private ShowMyInfoEvent() {}

    static ShowMyInfoEvent createEvent() {
        return new ShowMyInfoEvent();
    }
}
