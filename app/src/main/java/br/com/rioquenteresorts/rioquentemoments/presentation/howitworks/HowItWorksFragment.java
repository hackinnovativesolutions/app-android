package br.com.rioquenteresorts.rioquentemoments.presentation.howitworks;

import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseFragment;

/**
 * @author Filipe Bezerra
 */
public class HowItWorksFragment extends BaseFragment {

    public static final String TAG = HowItWorksFragment.class.getName();

    public static HowItWorksFragment newInstance() {
        return new HowItWorksFragment();
    }

    @Override protected int viewLayoutResource() {
        return R.layout.fragment_how_it_works;
    }
}
