package br.com.rioquenteresorts.rioquentemoments.presentation.myinfo;

import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseFragment;

/**
 * @author Filipe Bezerra
 */
public class MyInfoFragment extends BaseFragment {

    public static final String TAG = MyInfoFragment.class.getName();

    public static MyInfoFragment newInstance() {
        return new MyInfoFragment();
    }

    @Override protected int viewLayoutResource() {
        return R.layout.fragment_my_info;
    }
}
