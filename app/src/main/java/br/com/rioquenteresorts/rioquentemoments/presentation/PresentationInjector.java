package br.com.rioquenteresorts.rioquentemoments.presentation;

import android.content.Context;
import br.com.rioquenteresorts.rioquentemoments.ApplicationImpl;
import org.greenrobot.eventbus.EventBus;

import static br.com.rioquenteresorts.rioquentemoments.BuildConfig.DEBUG;

/**
 * @author Filipe Bezerra
 */
public class PresentationInjector {

    private PresentationInjector() {/* No instances */}

    private static EventBus sEventBus;

    public static EventBus provideEventBus() {
        if (sEventBus == null) {
            sEventBus = EventBus.builder()
                    .logNoSubscriberMessages(DEBUG)
                    .sendNoSubscriberEvent(DEBUG)
                    .build();
        }
        return sEventBus;
    }

    public static Context provideApplicationContext() {
        return ApplicationImpl.getInstance();
    }
}
