package br.com.rioquenteresorts.rioquentemoments.presentation.evaluation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.model.Attraction;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseFragment;
import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static br.com.rioquenteresorts.rioquentemoments.presentation.util.DrawableUtils.tint;

public class EvaluationFragment extends BaseFragment {

    public static final String TAG = EvaluationFragment.class.getName();
    public static final String EXTRA_ATTRACTION = TAG + ".extraAttraction";

    public static EvaluationFragment newInstance(final Attraction attraction) {
        EvaluationFragment fragment = new EvaluationFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(EXTRA_ATTRACTION, attraction);
        fragment.setArguments(arguments);
        return fragment;
    }

    @BindView(R.id.text_view_healine) TextView textViewHeadline;

    @BindView(R.id.image_view_liked) ImageView imageViewLiked;
    @BindView(R.id.text_view_liked) TextView textViewLiked;

    @BindView(R.id.image_view_not_liked) ImageView imageViewNotLiked;
    @BindView(R.id.text_view_not_like) TextView textViewNotLiked;

    @BindView(R.id.radio_group_liked_evaluation) RadioGroup radioGroupLikedEvaluation;
    @BindView(R.id.radio_group_not_liked_evaluation) RadioGroup radioGroupNotLikedEvaluation;

    @BindView(R.id.view_switcher) ViewSwitcher viewSwitcher;

    @BindView(R.id.input_layout_another_reason) TextInputLayout inputLayoutAnotherReason;

    private Attraction attraction;

    @Override protected int viewLayoutResource() {
        return R.layout.activity_evaluation;
    }

    @Override public void onViewCreated(final View view,
            @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadAttractionFromIntent();
    }

    private void loadAttractionFromIntent() {
        attraction = getArguments().getParcelable(EXTRA_ATTRACTION);
        textViewHeadline.setText(getString(R.string.headline_evaluation, attraction.getName()));
    }

    @OnClick(R.id.linear_layout_liked) void onLinearLayoutLikedClicked() {
        imageViewLiked.setImageDrawable(tint(getContext(), R.drawable.ic_thumb_up_black_24dp, R.color.md_teal_500));
        textViewLiked.setTextColor(ContextCompat.getColor(getContext(), R.color.md_teal_500));
        imageViewNotLiked.setImageDrawable(tint(getContext(), R.drawable.ic_thumb_down_black_24dp, R.color.md_grey_500));
        textViewNotLiked.setTextColor(ContextCompat.getColor(getContext(), R.color.md_grey_500));
        viewSwitcher.showNext();
    }

    @OnClick(R.id.linear_layout_not_liked) void onLinearLayoutNotLikedClicked() {
        imageViewLiked.setImageDrawable(tint(getContext(), R.drawable.ic_thumb_up_black_24dp, R.color.md_grey_500));
        textViewLiked.setTextColor(ContextCompat.getColor(getContext(), R.color.md_grey_500));
        imageViewNotLiked.setImageDrawable(tint(getContext(), R.drawable.ic_thumb_down_black_24dp, R.color.md_red_500));
        textViewNotLiked.setTextColor(ContextCompat.getColor(getContext(), R.color.md_red_500));
        viewSwitcher.showPrevious();
    }

    @OnCheckedChanged({
            R.id.radio_button_liked_another_one,
            R.id.radio_button_not_liked_another_one})
    void onRadioButtonAnotherOneChecked(boolean checked) {
        inputLayoutAnotherReason.getEditText().setEnabled(checked);
    }

    @OnClick(R.id.button_confirm) void onButtonConfirmClicked() {
        if (radioGroupLikedEvaluation.getCheckedRadioButtonId() == -1
                && radioGroupNotLikedEvaluation.getCheckedRadioButtonId() == -1) {
            Snackbar.make(getView(), "Gostou ou não gostou? Clique em uma opção acima!",
                    Snackbar.LENGTH_SHORT).show();
            return;
        }

        Snackbar.make(getView(), "Você ganhou 1 ponto!", Snackbar.LENGTH_LONG)
                .setAction("Meus pontos", new View.OnClickListener() {
                    @Override public void onClick(final View view) {
                        eventBus().postSticky(ShowMyInfoEvent.createEvent());
                    }
                })
                .addCallback(new Snackbar.Callback() {
                    @Override public void onDismissed(final Snackbar transientBottomBar,
                            final int event) {
                        eventBus().post(ShowEvaluateEvent.createEvent());
                    }
                })
                .show();
    }
}
