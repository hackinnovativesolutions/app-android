package br.com.rioquenteresorts.rioquentemoments.presentation.widget.recyclerview;

import android.view.View;

/**
 * @author Filipe Bezerra.
 */
public interface OnItemClickListener {

    void onSingleTapUp(View view, int position);

    void onLongPress(View view, int position);
}