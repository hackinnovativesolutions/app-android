package br.com.rioquenteresorts.rioquentemoments;

import android.app.Application;
import com.google.firebase.database.FirebaseDatabase;
import timber.log.Timber;

/**
 * @author Filipe Bezerra
 */
public class ApplicationImpl extends Application {

    private static ApplicationImpl mInstance;

    @Override public void onCreate() {
        super.onCreate();
        mInstance = this;
        initializeLogging();
        initializeFirebase();
    }

    private void initializeLogging() {
        Timber.plant(new Timber.DebugTree());
    }

    public static ApplicationImpl getInstance() {
        return mInstance;
    }

    private void initializeFirebase() {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
