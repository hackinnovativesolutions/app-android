package br.com.rioquenteresorts.rioquentemoments.presentation.visiting;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseActivity;
import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class VisitingActivity extends BaseActivity {

    @BindView(R.id.input_layout_another_reason) TextInputLayout inputLayoutAnotherReason;

    @Override protected int viewLayoutResource() {
        return R.layout.activity_visiting;
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnCheckedChanged(R.id.radio_button_another_one) void onRadioButtonAnotherOneChecked(
            boolean checked) {
        inputLayoutAnotherReason.getEditText().setEnabled(checked);
    }

    @OnClick(R.id.button_finish) void onButtonFinishClicked() {
        finish();
    }
}
