package br.com.rioquenteresorts.rioquentemoments.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import org.greenrobot.eventbus.EventBus;

/**
 * @author Filipe Bezerra
 */
public abstract class BaseFragment extends Fragment {

    private BaseActivity hostActivity;

    private Unbinder unbinder;

    public BaseFragment() {}

    @LayoutRes protected abstract int viewLayoutResource();

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        try {
            hostActivity = (BaseActivity) context;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Descendants of " + getClass().getName() +
                    " must be hosted by " + BaseActivity.class.getName());
        }
    }

    @Nullable @Override public View onCreateView(
            LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle inState) {
        final View fragmentView = inflater.inflate(viewLayoutResource(), container, false);
        unbinder = ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    protected Navigator navigate() {
        return hostActivity.navigate();
    }

    protected EventBus eventBus() {
        return hostActivity.eventBus();
    }
}
