package br.com.rioquenteresorts.rioquentemoments.presentation.login;

import android.content.Intent;
import android.os.Bundle;
import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseActivity;
import br.com.rioquenteresorts.rioquentemoments.presentation.visiting.VisitingActivity;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    @Override protected int viewLayoutResource() {
        return R.layout.activity_login;
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick(R.id.button_sign_up) void onButtonSignUpClicked() {
        startActivity(new Intent(this, VisitingActivity.class));
    }
}
