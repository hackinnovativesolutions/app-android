package br.com.rioquenteresorts.rioquentemoments.presentation.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import static android.graphics.PorterDuff.Mode.SRC_IN;
import static android.support.v4.content.ContextCompat.getColor;
import static android.support.v4.graphics.drawable.DrawableCompat.setTint;
import static android.support.v4.graphics.drawable.DrawableCompat.setTintMode;
import static android.support.v4.graphics.drawable.DrawableCompat.wrap;

/**
 * @author Filipe Bezerra
 */
public class DrawableUtils {

    private DrawableUtils() {}

    public static Drawable tint(
            @Nullable final Context context, @DrawableRes int drawableRes, @ColorRes int colorRes) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableRes);
        drawable = drawable.mutate();
        drawable = wrap(drawable);
        setTint(drawable, getColor(context, colorRes));
        setTintMode(drawable, SRC_IN);
        return drawable;
    }
}
