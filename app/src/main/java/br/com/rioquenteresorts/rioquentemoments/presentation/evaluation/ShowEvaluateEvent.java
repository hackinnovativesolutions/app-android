package br.com.rioquenteresorts.rioquentemoments.presentation.evaluation;

/**
 * @author Filipe Bezerra
 */
public class ShowEvaluateEvent {

    private ShowEvaluateEvent() {}

    static ShowEvaluateEvent createEvent() {
        return new ShowEvaluateEvent();
    }
}
