package br.com.rioquenteresorts.rioquentemoments.presentation.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseActivity;
import br.com.rioquenteresorts.rioquentemoments.presentation.evaluate.StartEvaluationEvent;
import br.com.rioquenteresorts.rioquentemoments.presentation.evaluation.ShowEvaluateEvent;
import br.com.rioquenteresorts.rioquentemoments.presentation.evaluation.ShowMyInfoEvent;
import br.com.rioquenteresorts.rioquentemoments.presentation.generalevaluation.ShowThanksEvent;
import br.com.rioquenteresorts.rioquentemoments.presentation.login.LoginActivity;
import butterknife.BindView;
import butterknife.OnClick;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import java.util.List;
import org.greenrobot.eventbus.Subscribe;

import static android.support.graphics.drawable.VectorDrawableCompat.create;
import static br.com.rioquenteresorts.rioquentemoments.presentation.preferences.PreferencesHelper.isUserLearnedHowItWorks;
import static br.com.rioquenteresorts.rioquentemoments.presentation.preferences.PreferencesHelper.setLearnedHowItWorks;
import static java.util.Collections.singletonList;

public class MainActivity extends BaseActivity implements Drawer.OnDrawerItemClickListener {

    private static final int DRAWER_ITEM_MY_INFO = 1;
    private static final int DRAWER_ITEM_EVALUATE = 2;
    private static final int DRAWER_ITEM_HOW_IT_WORKS = 3;
    private static final int DRAWER_ITEM_GENERAL_EVALUATION = 4;
    private static final int DRAWER_ITEM_LOGOUT = 5;

    @BindView(R.id.fab) FloatingActionButton floatingActionButton;

    private Drawer drawer;
    private AccountHeader accountHeader;
    private PrimaryDrawerItem drawerItemHowItWorks;
    private PrimaryDrawerItem drawerItemEvaluate;
    private PrimaryDrawerItem drawerItemMyInfo;
    private PrimaryDrawerItem drawerItemGeneralEvaluation;
    private PrimaryDrawerItem drawerItemLogout;

    @Override protected int viewLayoutResource() {
        return R.layout.activity_main;
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDrawerHeader(savedInstanceState);
        initializeDrawer(savedInstanceState);

        startActivity(new Intent(this, LoginActivity.class));

        if (isUserLearnedHowItWorks()) {
            navigateToEvaluate();
        } else {
            navigate().toHowItWorks();
            setLearnedHowItWorks();
        }
    }

    private void initializeDrawerHeader(final Bundle inState) {
        accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withProfiles(createUserProfile())
                .withHeaderBackground(R.drawable.main_header_background)
                .withSavedInstance(inState)
                .withSelectionListEnabledForSingleProfile(false)
                .build();
    }

    private List<IProfile> createUserProfile() {
        final IProfile userProfile = new ProfileDrawerItem()
                .withName("Vandré Sales")
                .withEmail("0 pontos")
                .withIcon(R.drawable.perfil_vandre_sales);
        return singletonList(userProfile);
    }

    private void initializeDrawer(final Bundle inState) {
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader)
                .addDrawerItems(
                        createDrawerItemHowItWorks(),
                        createDrawerItemEvaluate(),
                        createDrawerItemGeneralEvaluation(),
                        createDrawerItemMyInfo(),
                        createDrawerItemLogout()
                )
                .withSavedInstance(inState)
                .withShowDrawerOnFirstLaunch(true)
                .withOnDrawerItemClickListener(this)
                .build();
    }

    private PrimaryDrawerItem createDrawerItemEvaluate() {
        if (drawerItemEvaluate == null) {
            drawerItemEvaluate = new PrimaryDrawerItem()
                    .withIdentifier(DRAWER_ITEM_EVALUATE)
                    .withName(R.string.drawer_item_rate)
                    .withIcon(create(getResources(), R.drawable.ic_star_black_24dp, getTheme()))
                    .withSelectedIconColorRes(R.color.color_primary);
        }
        return drawerItemEvaluate;
    }

    private PrimaryDrawerItem createDrawerItemMyInfo() {
        if (drawerItemMyInfo == null) {
            drawerItemMyInfo = new PrimaryDrawerItem()
                    .withIdentifier(DRAWER_ITEM_MY_INFO)
                    .withName(R.string.drawer_item_my_info)
                    .withIcon(create(getResources(), R.drawable.ic_info_black_24dp, getTheme()))
                    .withSelectedIconColorRes(R.color.color_primary);
        }
        return drawerItemMyInfo;
    }

    private PrimaryDrawerItem createDrawerItemHowItWorks() {
        if (drawerItemHowItWorks == null) {
            drawerItemHowItWorks = new PrimaryDrawerItem()
                    .withIdentifier(DRAWER_ITEM_HOW_IT_WORKS)
                    .withName(R.string.drawer_item_how_it_works)
                    .withIcon(create(getResources(), R.drawable.ic_help_black_24dp, getTheme()))
                    .withSelectedIconColorRes(R.color.color_primary);
        }
        return drawerItemHowItWorks;
    }

    private PrimaryDrawerItem createDrawerItemGeneralEvaluation() {
        if (drawerItemGeneralEvaluation == null) {
            drawerItemGeneralEvaluation = new PrimaryDrawerItem()
                    .withIdentifier(DRAWER_ITEM_GENERAL_EVALUATION)
                    .withName(R.string.drawer_item_general_rate)
                    .withIcon(create(getResources(), R.drawable.ic_rate_review_black_24dp, getTheme()))
                    .withSelectedIconColorRes(R.color.color_primary);
        }
        return drawerItemGeneralEvaluation;
    }

    private PrimaryDrawerItem createDrawerItemLogout() {
        if (drawerItemLogout == null) {
            drawerItemLogout = new PrimaryDrawerItem()
                    .withIdentifier(DRAWER_ITEM_LOGOUT)
                    .withName(R.string.drawer_item_logout)
                    .withIcon(create(getResources(), R.drawable.ic_exit_to_app_black_24dp, getTheme()))
                    .withSelectedIconColorRes(R.color.color_primary);
        }
        return drawerItemLogout;
    }

    @Override public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        if (drawer != null) {
            outState = drawer.saveInstanceState(outState);
        }
        if (accountHeader != null) {
            outState = accountHeader.saveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override public boolean onItemClick(
            final View view, final int position, final IDrawerItem drawerItem) {
        switch ((int)drawerItem.getIdentifier()) {
            case DRAWER_ITEM_HOW_IT_WORKS: {
                floatingActionButton.show();
                navigate().toHowItWorks();
                break;
            }
            case DRAWER_ITEM_EVALUATE: {
                navigateToEvaluate();
                break;
            }
            case DRAWER_ITEM_GENERAL_EVALUATION: {
                floatingActionButton.hide();
                navigate().toGeneralRate();
                break;
            }
            case DRAWER_ITEM_MY_INFO: {
                navigateToMyInfo();
                break;
            }
            case DRAWER_ITEM_LOGOUT: {
                startActivity(new Intent(this, LoginActivity.class));
                break;
            }
        }
        return false;
    }

    @Override protected void onStart() {
        if (!eventBus().isRegistered(this)) {
            eventBus().register(this);
        }
        super.onStart();
    }

    @Override protected void onStop() {
        if (eventBus().isRegistered(this)) {
            eventBus().unregister(this);
        }
        super.onStop();
    }

    @Subscribe public void onShowMyInfoEvent(ShowMyInfoEvent event) {
        eventBus().removeStickyEvent(event);
        navigateToMyInfo();
    }

    private void navigateToMyInfo() {
        floatingActionButton.show();
        final long currentSelection = drawer.getCurrentSelection();
        if (currentSelection != DRAWER_ITEM_MY_INFO) {
            drawer.setSelection(DRAWER_ITEM_MY_INFO);
        }
        navigate().toMyInfo();
    }

    @Subscribe public void onShowThanksEvent(ShowThanksEvent event) {
        eventBus().removeStickyEvent(event);
        navigate().toThanks();
    }

    @OnClick(R.id.fab) void onFabClicked() {
        navigateToEvaluate();
    }

    private void navigateToEvaluate() {
        floatingActionButton.hide();
        final long currentSelection = drawer.getCurrentSelection();
        if (currentSelection != DRAWER_ITEM_EVALUATE) {
            drawer.setSelection(DRAWER_ITEM_EVALUATE);
        }
        navigate().toEvaluate();
    }

    @Subscribe public void onStartEvaluationEvent(StartEvaluationEvent event) {
        eventBus().removeStickyEvent(event);
        navigate().toEvaluation(event.getAttraction());
    }

    @Subscribe public void onShowEvaluateEvent(ShowEvaluateEvent event) {
        eventBus().removeStickyEvent(event);
        navigate().toEvaluate();
    }
}
