package br.com.rioquenteresorts.rioquentemoments.presentation.evaluate;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.model.Attraction;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseFragment;
import br.com.rioquenteresorts.rioquentemoments.presentation.widget.recyclerview.OnItemClickListener;
import br.com.rioquenteresorts.rioquentemoments.presentation.widget.recyclerview.OnItemTouchListener;
import butterknife.BindView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import timber.log.Timber;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;

/**
 * @author Filipe Bezerra
 */
public class EvaluateFragment extends BaseFragment {

    private static final String ATTRACTIONS_CHILD = "attractions";
    public static final String TAG = EvaluateFragment.class.getName();

    public static EvaluateFragment newInstance() {
        return new EvaluateFragment();
    }

    @BindView(R.id.recycler_view_attractions) RecyclerView recyclerViewAttractions;


    private DatabaseReference firebaseDatabaseReference;
    private FirebaseRecyclerAdapter<Attraction, AttractionsViewHolder> firebaseAdapter;

    @Override protected int viewLayoutResource() {
        return R.layout.fragment_evaluate;
    }

    @Override public void onViewCreated(
            final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseAdapter = new FirebaseRecyclerAdapter<Attraction, AttractionsViewHolder>(
                Attraction.class,
                R.layout.list_item_attraction,
                AttractionsViewHolder.class,
                firebaseDatabaseReference.child(ATTRACTIONS_CHILD)) {
            @Override protected void populateViewHolder(
                    final AttractionsViewHolder holder,
                    final Attraction attraction,
                    final int position) {
                holder.textViewAttractionName.setText(attraction.getName());
                holder.textViewAttractionLocation.setText(attraction.getLocation());

                final String imageUrl = attraction.getImageUrl();
                if (imageUrl.startsWith("gs://")) {
                    StorageReference storageReference = FirebaseStorage.getInstance()
                            .getReferenceFromUrl(imageUrl);
                    storageReference.getDownloadUrl()
                            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override public void onComplete(@NonNull final Task<Uri> task) {
                                    if (task.isSuccessful()) {
                                        String downloadUrl = task.getResult().toString();
                                        Picasso.with(getContext())
                                                .load(downloadUrl)
                                                .placeholder(R.drawable.rio_quente_resorts_logo)
                                                .fit()
                                                .centerCrop()
                                                .into(holder.imageViewAttractionImage);
                                    } else {
                                        Timber.w("Getting download url was not successful.",
                                                task.getException());
                                    }
                                }
                            });
                } else {
                    Picasso.with(getContext())
                            .load(imageUrl)
                            .placeholder(R.drawable.rio_quente_resorts_logo)
                            .fit()
                            .centerCrop()
                            .into(holder.imageViewAttractionImage);
                }
            }
        };

        recyclerViewAttractions.setHasFixedSize(true);
        recyclerViewAttractions.setAdapter(firebaseAdapter);
        recyclerViewAttractions.addItemDecoration(new DividerItemDecoration(getContext(), VERTICAL));
        recyclerViewAttractions.addOnItemTouchListener(new OnItemTouchListener(getContext(),
                recyclerViewAttractions, new OnItemClickListener() {
            @Override public void onSingleTapUp(final View view, final int position) {
                final Attraction attraction = firebaseAdapter.getItem(position);
                eventBus().post(StartEvaluationEvent.createEvent(attraction));
            }

            @Override public void onLongPress(final View view, final int position) {}
        }));
    }
}
