package br.com.rioquenteresorts.rioquentemoments.presentation.generalevaluation;

/**
 * @author Filipe Bezerra
 */
public class ShowThanksEvent {

    private ShowThanksEvent() {}

    static ShowThanksEvent createEvent() {
        return new ShowThanksEvent();
    }
}
