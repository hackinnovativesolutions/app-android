package br.com.rioquenteresorts.rioquentemoments.presentation.thanks;

import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseFragment;

/**
 * @author Filipe Bezerra
 */
public class ThanksFragment extends BaseFragment {

    public static final String TAG = ThanksFragment.class.getName();

    public static ThanksFragment newInstance() {
        return new ThanksFragment();
    }

    @Override protected int viewLayoutResource() {
        return R.layout.fragment_thanks;
    }
}
