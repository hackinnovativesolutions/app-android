package br.com.rioquenteresorts.rioquentemoments.presentation.evaluate;

import br.com.rioquenteresorts.rioquentemoments.model.Attraction;

/**
 * @author Filipe Bezerra
 */
public class StartEvaluationEvent {

    private Attraction attraction;

    private StartEvaluationEvent(final Attraction attraction) {
        this.attraction = attraction;
    }

    static StartEvaluationEvent createEvent(final Attraction attraction) {
        return new StartEvaluationEvent(attraction);
    }

    public Attraction getAttraction() {
        return attraction;
    }
}
