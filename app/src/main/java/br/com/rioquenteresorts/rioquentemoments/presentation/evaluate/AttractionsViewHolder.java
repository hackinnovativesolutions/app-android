package br.com.rioquenteresorts.rioquentemoments.presentation.evaluate;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.rioquenteresorts.rioquentemoments.R;
import butterknife.BindView;

import static butterknife.ButterKnife.bind;

/**
 * @author Filipe Bezerra
 */
class AttractionsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.image_view_attraction_image) ImageView imageViewAttractionImage;
    @BindView(R.id.text_view_attraction_name) TextView textViewAttractionName;
    @BindView(R.id.text_view_attraction_location) TextView textViewAttractionLocation;

    AttractionsViewHolder(final View itemView) {
        super(itemView);
        bind(this, itemView);
    }
}
