package br.com.rioquenteresorts.rioquentemoments.presentation.preferences;

import android.annotation.SuppressLint;
import android.support.v4.content.SharedPreferencesCompat;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;
import static br.com.rioquenteresorts.rioquentemoments.presentation.PresentationInjector.provideApplicationContext;

/**
 * @author Filipe Bezerra
 */
public class PreferencesHelper {

    private static final String USER_LEARNED_HOW_IT_WORKS = "pref-UserLearnedHowItWorks";

    private PreferencesHelper() {}

    public static boolean isUserLearnedHowItWorks() {
        return getDefaultSharedPreferences(provideApplicationContext())
                .getBoolean(USER_LEARNED_HOW_IT_WORKS, false);
    }

    @SuppressLint("CommitPrefEdits") public static void setLearnedHowItWorks() {
        SharedPreferencesCompat.EditorCompat
                .getInstance()
                .apply(getDefaultSharedPreferences(provideApplicationContext())
                        .edit()
                        .putBoolean(USER_LEARNED_HOW_IT_WORKS, true));

    }
}
