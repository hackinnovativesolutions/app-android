package br.com.rioquenteresorts.rioquentemoments.presentation.generalevaluation;

import android.widget.ImageView;
import android.widget.TextView;
import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.presentation.base.BaseFragment;
import butterknife.BindView;
import butterknife.OnClick;

import static android.support.v4.content.ContextCompat.getColor;
import static br.com.rioquenteresorts.rioquentemoments.presentation.util.DrawableUtils.tint;

/**
 * @author Filipe Bezerra
 */
public class GeneralEvaluationFragment extends BaseFragment {

    public static final String TAG = GeneralEvaluationFragment.class.getName();

    @BindView(R.id.image_view_liked) ImageView imageViewLiked;
    @BindView(R.id.text_view_liked) TextView textViewLiked;

    @BindView(R.id.image_view_not_liked) ImageView imageViewNotLiked;
    @BindView(R.id.text_view_not_like) TextView textViewNotLiked;

    public static GeneralEvaluationFragment newInstance() {
        return new GeneralEvaluationFragment();
    }

    @Override protected int viewLayoutResource() {
        return R.layout.fragment_general_evaluation;
    }

    @OnClick(R.id.linear_layout_liked) void onLinearLayoutLikedClicked() {
        imageViewLiked.setImageDrawable(tint(getContext(), R.drawable.ic_thumb_up_black_24dp, R.color.md_teal_500));
        textViewLiked.setTextColor(getColor(getContext(), R.color.md_teal_500));
        imageViewNotLiked.setImageDrawable(tint(getContext(), R.drawable.ic_thumb_down_black_24dp, R.color.md_grey_500));
        textViewNotLiked.setTextColor(getColor(getContext(), R.color.md_grey_500));
    }

    @OnClick(R.id.linear_layout_not_liked) void onLinearLayoutNotLikedClicked() {
        imageViewLiked.setImageDrawable(tint(getContext(), R.drawable.ic_thumb_up_black_24dp, R.color.md_grey_500));
        textViewLiked.setTextColor(getColor(getContext(), R.color.md_grey_500));
        imageViewNotLiked.setImageDrawable(tint(getContext(), R.drawable.ic_thumb_down_black_24dp, R.color.md_red_500));
        textViewNotLiked.setTextColor(getColor(getContext(), R.color.md_red_500));
    }

    @OnClick(R.id.button_confirm) void onButtonConfirmClicked() {
        eventBus().postSticky(ShowThanksEvent.createEvent());
    }
}
