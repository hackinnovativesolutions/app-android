package br.com.rioquenteresorts.rioquentemoments.presentation.base;

import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.model.Attraction;
import br.com.rioquenteresorts.rioquentemoments.presentation.evaluate.EvaluateFragment;
import br.com.rioquenteresorts.rioquentemoments.presentation.evaluation.EvaluationFragment;
import br.com.rioquenteresorts.rioquentemoments.presentation.generalevaluation.GeneralEvaluationFragment;
import br.com.rioquenteresorts.rioquentemoments.presentation.howitworks.HowItWorksFragment;
import br.com.rioquenteresorts.rioquentemoments.presentation.myinfo.MyInfoFragment;
import br.com.rioquenteresorts.rioquentemoments.presentation.thanks.ThanksFragment;

/**
 * @author Filipe Bezerra
 */
public class Navigator {

    private final BaseActivity activity;

    Navigator(final BaseActivity activity) {
        this.activity = activity;
    }

    public void toHowItWorks() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        HowItWorksFragment.newInstance(), HowItWorksFragment.TAG)
                .commit();
        activity.setTitle(R.string.drawer_item_how_it_works);
    }

    public void toEvaluate() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        EvaluateFragment.newInstance(), EvaluateFragment.TAG)
                .commit();
        activity.setTitle(R.string.title_fragment_evaluate);
    }

    public void toGeneralRate() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        GeneralEvaluationFragment.newInstance(), GeneralEvaluationFragment.TAG)
                .commit();
        activity.setTitle(R.string.drawer_item_general_rate);
    }

    public void toMyInfo() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        MyInfoFragment.newInstance(), MyInfoFragment.TAG)
                .commit();
        activity.setTitle(R.string.drawer_item_my_info);
    }

    public void toEvaluation(final Attraction attraction) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        EvaluationFragment.newInstance(attraction), EvaluationFragment.TAG)
                .commit();
        activity.setTitle(R.string.title_fragment_evaluation);
    }

    public void toThanks() {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container,
                        ThanksFragment.newInstance(), ThanksFragment.TAG)
                .commit();
        activity.setTitle(R.string.title_fragment_thanks);
    }
}
