package br.com.rioquenteresorts.rioquentemoments.presentation.evaluate;

import br.com.rioquenteresorts.rioquentemoments.model.Attraction;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Filipe Bezerra
 */
class Attractions {

    private static final List<Attraction> data;

    static {
        List<Attraction> temp = Arrays.asList(
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715"),
                new Attraction("Xpirado", "Vila das Palmas, Hot Park", "https://firebasestorage.googleapis.com/v0/b/rio-quente-moments.appspot.com/o/attractions%2Fxpirado_01_large.jpg?alt=media&token=f33ba4df-b248-4dbf-a1f9-24b704ee1715")
        );
        data = Collections.unmodifiableList(temp);
    }

    static List<Attraction> list() {
        return Collections.unmodifiableList(data);
    }
}
