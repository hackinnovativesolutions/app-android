package br.com.rioquenteresorts.rioquentemoments.presentation.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import br.com.rioquenteresorts.rioquentemoments.R;
import br.com.rioquenteresorts.rioquentemoments.presentation.PresentationInjector;
import butterknife.BindView;
import org.greenrobot.eventbus.EventBus;

import static android.support.v7.app.AppCompatDelegate.setCompatVectorFromResourcesEnabled;
import static butterknife.ButterKnife.bind;

/**
 * @author Filipe Bezerra
 */
public abstract class BaseActivity extends AppCompatActivity {

    static {
        setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.toolbar) @Nullable protected Toolbar toolbar;

    private Navigator navigator;

    @Override protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(viewLayoutResource());
        bind(this);
        navigator = new Navigator(this);

        if (toolbar != null)
            setSupportActionBar(toolbar);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (getParentActivityIntent() == null) {
                onBackPressed();
            } else {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void setAsSubActivity() {
        setupToolbar(R.drawable.ic_arrow_back_white_24dp);
    }

    @SuppressWarnings("ConstantConditions")
    private void setupToolbar(int drawableRes) {
        if (toolbar != null) {
            if (getSupportActionBar() == null) {
                setSupportActionBar(toolbar);
            }
            getSupportActionBar().setHomeAsUpIndicator(drawableRes);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @LayoutRes protected abstract int viewLayoutResource();

    protected Navigator navigate() {
        if (navigator == null) {
            navigator = new Navigator(this);
        }
        return navigator;
    }

    protected void setTitle(@Nullable String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    protected EventBus eventBus() {
        return PresentationInjector.provideEventBus();
    }
}
